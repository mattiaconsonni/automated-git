import org.apache.commons.exec.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GitAutomatico {

    private static final String url = "jdbc:mysql://localhost:3306/db_git";
    private static final String user = "root";
    private static final String password = "root";
    private static final Logger logger = LogManager.getLogger();
    private static int id_table = 1;
    private static final String GIT_ADD_COMMAND = "git add .";
    private static final String GIT_COMMIT_COMMAND = "git commit -m \"Aggiornamento automatico\"";
    private static final String GIT_PUSH_COMMAND = "git push";
    public static void main(String[] args) {
        // Cartella che viene monitorata.
        String directoryPath = "C:\\Users\\mconsonn\\gitProva";
        // Inizializzazione della cartella monitorata.
        AggiornamentoDirectory.inizializzaCartellaMonitorata(directoryPath);
        while (true) {
            // Controllo cambiamenti nella directory.
            if (AggiornamentoDirectory.controllaModificaCartella(directoryPath)) {
                try {
                    eseguiComando(GIT_ADD_COMMAND, directoryPath);
                    eseguiComando(GIT_COMMIT_COMMAND, directoryPath);
                    eseguiComando(GIT_PUSH_COMMAND, directoryPath);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            // Periodo di pausa tra gli aggiornamenti.
            try {
                Thread.sleep(10000); // 10 secondi
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    //Metodo che esegue il comando di Git e aggiorna il database
    private static void eseguiComando(String comando, String directoryPath) throws ExecuteException, IOException {
        // Blocco di codice che gestisce gli inserimenti nel database.
        try {
            String sql = "INSERT INTO db_git.git_operations_log (id, operation) VALUES (?, ?)";
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id_table++);
            statement.setString(2, comando);
            int rowsInserted = statement.executeUpdate();
            logger.info("Database aggiornato!");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Blocco di codice che si occupa di parsare il comando passato in input.
        CommandLine cmdLine = CommandLine.parse(comando);
        DefaultExecutor executor = new DefaultExecutor();
        // Seleziione della directory su cui si vuole lavorare.
        executor.setWorkingDirectory(new java.io.File(directoryPath));
        try {
            // Valore di uscita dell'operazione.
            int exitValue = executor.execute(cmdLine);
            System.out.println("Aggiunta dei file eseguita con codice di uscita: " + exitValue);
        } catch (ExecuteException e) {
            System.err.println("Esecuzione fallita: " + e);
        } catch (IOException e) {
            System.err.println("IOException: " + e);
        }

    }
}
