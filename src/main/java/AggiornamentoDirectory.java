import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AggiornamentoDirectory {
        // Hashmap per la corrispondenza tra il file e il suo hash.
        private static Map<File, String> fileCartella = new HashMap<>();
        private static final Logger logger = LogManager.getLogger();
        // Metodo che inizializza la cartella da monitorare.
        public static void inizializzaCartellaMonitorata(String nomeCartella) {
            logger.info("Directory impostata!");
            // Crea un file rappresentante il nome della cartella
            File cartella = new File(nomeCartella);
            // Ottiene la lista di file e cartelle presenti.
            File[] files = cartella.listFiles();
            aggiornaCartella(files);
        }
        // Metodo che aggiorna la cartella dopo una modifica.
        private static void aggiornaCartella(File[] files) {
            for (File file : files) {
                try {
                    // Non viene eseguito l'hash delle cartelle.
                    if (file.isDirectory())
                        fileCartella.put(file, null);
                    else
                        // Ogni file viene hashato con SHA-256
                        fileCartella.put(file, calculateSHA256(file));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            logger.info("Directory aggiornata!");
        }
        // Metodo che si occupa di hashare un file utilizzando la funzione hash SHA-256.
        public static String calculateSHA256(File file) throws IOException {
            try {
                // Crea un istanza di MessageDigest di SHA-256
                MessageDigest digest = MessageDigest.getInstance("SHA-256");

                // Apertura del file input stream.
                FileInputStream fis = new FileInputStream(file);

                // Leggi il contenuto dei file in blocchi da 8KB.
                byte[] buffer = new byte[8192];
                int bytesRead;
                while ((bytesRead = fis.read(buffer)) != -1) {
                    // Aggiornamento del message digest con i byte letti.
                    digest.update(buffer, 0, bytesRead);
                }

                // Chiusura dello stream.
                fis.close();

                // Calcolo del hahs.
                byte[] hashBytes = digest.digest();

                // Conversione del array di byte in una stringa esadecimale.
                StringBuilder hexString = new StringBuilder();
                for (byte hashByte : hashBytes) {
                    String hex = Integer.toHexString(0xff & hashByte);
                    if (hex.length() == 1) {
                        hexString.append('0');
                    }
                    hexString.append(hex);
                }
                return hexString.toString();
            } catch (NoSuchAlgorithmException e) {
                // Gestisce l'eccezione NoSuchAlgorithm.
                e.printStackTrace();
                return null;
            }
        }

        public static boolean controllaModificaCartella(String path) {
            // Creazione della cartella aggiornata.
            File directory = new File(path);
            // Ottengo la lista dei file della cartella.-
            File[] files = directory.listFiles();
            // Controllo se sono stati aggiunti dei file.
            if (files.length > fileCartella.size()) {
                logger.info("File aggiunto!");
                aggiornaCartella(files);
                return true;
            }
            // Controllo se i file sono stati modificati.
            for (File file : files) {
                try {
                    // Non controllo le directory.
                    if (file.isDirectory())
                        continue;
                    // Controllo se gli hash dei file corrispondono o meno.
                    if (!fileCartella.get(file).equals(calculateSHA256(file))) {
                        logger.info("File modificato!");
                        System.out.println("File " + file.getAbsolutePath() + " cambiato!");
                        aggiornaCartella(files);
                        return true;
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return false;
        }
}

